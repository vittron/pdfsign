<?php

namespace app\modules\pdfsign;

use Yii;
use yii\base\Module;

/**
 * Module that contains Signer functionality
 */
class PdfSigner extends Module
{
    /**
     * Init module
     */
    public function init()
    {
        parent::init();
    }
}