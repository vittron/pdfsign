<?php

namespace app\modules\pdfsign\components;


use yii\base\Component;
use yii\base\Exception;

class SignManager extends Component
{
    // public static $curl;

    public static $signServerUrl = 'https://sign.calresults.net/worker/PDFSigner';

    public static function sign($pdfModel)
    {
        $filePath = \Yii::getAlias('@app/uploads/') . $pdfModel->pdfFile;
        $curl = curl_init(self::$signServerUrl);
        $headers = [
            'Content-Type: multipart/form-data',
            // 'Content-Length: ' . filesize(realpath($filePath)),
        ];
        $postData = [
            'filerecievefile' => '@' . realpath($filePath),
            'REQUEST_METADATA.pdfPassword' => '',
            'REQUEST_METADATA' => '',
        ];
        $options = [
            // CURLOPT_HEADER => 1,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => $postData,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_VERBOSE => 1,
        ];
        curl_setopt_array($curl, $options);
        $result = curl_exec($curl);
        if(curl_errno($curl)){
            throw new Exception(curl_error($curl));
        }
        curl_close($curl);

        return $result;
    }

    public static function sendSignedFile($pdfModel, $fileContent)
    {
        $signedFileName = $pdfModel->pdfFile->baseName . '_signed.pdf';

        // send signed PDF file to the user
        header('Content-Type: application/pdf'); // you can change this based on the file type
        header('Content-Disposition: attachment; filename="' . $signedFileName . '"');
        echo $fileContent;

        return true;
    }
}