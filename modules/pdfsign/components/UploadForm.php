<?php

namespace app\modules\pdfsign\components;

use yii\base\Model;

class UploadForm extends Model
{
    public $pdfFile;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['pdfFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'pdf']
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'pdfFile' => \Yii::t('app', 'PDF file to upload'),
        ];
    }

    /**
     * @return bool
     */
    public function upload()
    {
        if ($this->validate()) {
            $this->pdfFile->saveAs(\Yii::getAlias('@app/uploads/') . $this->pdfFile->baseName . '.pdf');
            return true;
        } else {
            return false;
        }
    }
}