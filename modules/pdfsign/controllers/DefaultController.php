<?php

namespace app\modules\pdfsign\controllers;

use app\modules\pdfsign\components\SignManager;
use app\modules\pdfsign\components\UploadForm;
use Yii;
use yii\data\ArrayDataProvider;
use yii\web\Controller;
use yii\web\UploadedFile;

/**
 * Default controller for the `git` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $model = new UploadForm();

        if (Yii::$app->request->isPost) {
            $model->pdfFile = UploadedFile::getInstance($model, 'pdfFile');
            if ($model->upload()) {
                if ($signedFile = SignManager::sign($model)) {
                    // return file to user
                    SignManager::sendSignedFile($model, $signedFile);
                    Yii::$app->end();
                }
            }
        }

        return $this->render('index', [
            'model' => $model,
        ]);
    }
}